<?php

namespace App\Http\Controllers\Admin\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\ProductRequest;
use Illuminate\Support\Facades\DB;
use Validator;

use App\Models\Admin\Product\Product;
use App\Models\Admin\Product\Category;
use App\Models\Admin\Brand\Brand;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product = Product::paginate(10);
        $category = Category::all();
        $brand = Brand::all();
        // dd($product);
        return view('admin/pages/product.view-product', [
            'data' => $product,
            'category' => $category,
            'brand' => $brand
        ]);

        // return response()->json([
        //     'data' => $product,
        // ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'category_id' => 'required',
            'brand_id' => 'required',
            'name' => 'required|min:10|max:150',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif',
        ]);

        if ($validation->passes()) {
            $image = $request->file('image');
            $main_image = time() . '_' . rand() . '_' . $image->getClientOriginalName();
            $des_path = public_path('images/product');
            $image->move($des_path, $main_image);
            
            $product = new Product;
            $product->category_id = $request->category_id;
            $product->brand_id = $request->brand_id;
            $product->name = $request->name;
            $product->main_image = $main_image;
            $product->price = $request->price;
            $product->description = $request->description;

            $product->save();
            return response()->json([
                'data' => $product,
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = DB::table('products')->find($id);
        // dd($product);
        return view('admin/pages/product.detail-product', [
            'data' => $product,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = DB::table('products')->find($id);
        $des_path = 'images/product/'.$product->main_image;
        if(file_exists($des_path)) {
            unlink($des_path);
        }
        Product::find($id)->delete();
        // return redirect()->route('product.index');
        // return response();
    }
}
