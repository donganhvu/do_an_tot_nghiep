<?php

namespace App\Models\Admin\Brand;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $table = 'brands';
    protected $primaryKey = 'id';

    protected $fillable = ['id', 'name_brand', 'image_brand', 'views', 'status'];

    public function productSet()
    {
        return $this->hasMany(App\Models\Admin\Product\ProductSet::class, 'brand_id');
    }
}
