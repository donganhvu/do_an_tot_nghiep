<?php

namespace App\Models\Admin\Product;

use Illuminate\Database\Eloquent\Model;

class DetailImage extends Model
{
    protected $table = "detail_images";

    protected $primaryKey = "id";

    protected $fillable = [
        'id', 'product_id', 'sub_image', 'status'
    ];
}
