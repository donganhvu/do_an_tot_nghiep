<?php

namespace App\Models\Admin\Product;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = "products";

    protected $primaryKey = "id";

    protected $fillable = [
        'id', 'category_id', 'brand_id', 'name', 'main_image', 'price', 'description', 'sale', 'views', 'status'
    ];
}
