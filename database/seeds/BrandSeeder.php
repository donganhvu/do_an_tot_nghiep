<?php

use Illuminate\Database\Seeder;
use App\Models\Admin\Brand\Brand;
use App\Models\Admin\Product\ProductSet;

class BrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // for ($i = 1; $i <= 50; $i++) {
        //     $brand = new Brand;
        //     $brand->name_brand = 'thuong hieu '.$i;
        //     $brand->image_brand = 'anh thuong hieu'.$i;
        //     $brand->save();
        // }
        

        factory(ProductSet::class, 30)->create()->each(function ($productSet) {
            $productSet->contacts()->save(factory(Brand::class)->make());
        });
        
    }
}
