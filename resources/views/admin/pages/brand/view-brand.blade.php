@extends('admin-index')
@section('content')
    <div class="container-fluid mt-3">
        <div class="row">
            <div class="col-md-12">
                <!-- Button -->
                <div class="d-flex">
                    <button class="btn btn-primary btn-round mb-3" data-toggle="modal" data-target="#addRowModal">
                        <i class="fa fa-plus"></i> Thêm mới thương hiệu
                    </button>
                </div>

                <!-- Modal -->
                <div class="modal fade" id="addRowModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                    aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header no-bd">
                                <h5 class="modal-title">
                                    <span class="fw-mediumbold">
                                        Thêm mới</span>
                                    <span class="fw-light text-uppercase">
                                        Thương hiệu
                                    </span>
                                </h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p class="small">Create a new row using this form, make sure you fill them all</p>
                                <form role="form">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group form-group-default">
                                                <label>Name</label>
                                                <input id="addName" type="text" class="form-control"
                                                    placeholder="fill name">
                                            </div>
                                        </div>
                                        <div class="col-md-6 pr-0">
                                            <div class="form-group form-group-default">
                                                <label>Position</label>
                                                <input id="addPosition" type="text" class="form-control"
                                                    placeholder="fill position">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group form-group-default">
                                                <label>Office</label>
                                                <input id="addOffice" type="text" class="form-control"
                                                    placeholder="fill office">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer no-bd">
                                <button type="button" id="addRowButton" class="btn btn-primary">Add</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Table -->
                <div class="table-responsive">
                    <table id="example" class="table table-striped table-bordered text-center table-hover" style="width:100%">
                        <thead>
                            <tr>
                                <th>STT</th>
                                <th>Tên thương hiệu</th>
                                <th>Ảnh thương hiệu</th>
                                <th>Lượt xem</th>
                                <th>Trạng thái</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $count = 1
                            @endphp
                            @foreach ($data as $value)
                                <tr>
                                    <td>{{ $count++ }}</td>
                                    <td>{{ $value['name_brand'] }}</td>
                                    <td>{{ $value['image_brand'] }}</td>
                                    <td>{{ $value['views'] }}</td>
                                    <td>{{ $value['status'] }}</td>
                                    <td>
                                        <div class="d-flex">
                                            <button class="btn" type="button"><i class="fas fa-eye text-info font-20"></i></button>
                                            <button class="btn ml-2 mr-2" type="button"><i class="fas fa-edit text-warning"></i></button>
                                            <button class="btn" type="button"><i class="fas fa-trash-alt text-danger"></i></button>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>STT</th>
                                <th>Tên thương hiệu</th>
                                <th>Ảnh thương hiệu</th>
                                <th>Lượt xem</th>
                                <th>Trạng thái</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        });

    </script>
@endsection
