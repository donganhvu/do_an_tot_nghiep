<div class="modal-header no-bd">
    <h5 class="modal-title">
        <span class="fw-mediumbold">
            Chi tiết</span>
        <span class="text-uppercase font-weight-bold text-info">
            Sản phẩm
        </span>
    </h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-sm-4 col-md-4">
            <h3 class="text-center">Sản phẩm</h3>
            <img src="{{ asset('images/product/' . $data->main_image) }}" style="width:100%; height: 400px;"
                alt="">
        </div>
        <div class="col-sm-8 col-md-8">
            <h3 class="text-center">Thông tin sản phẩm</h3>
        </div>
    </div>
</div>
<div class="modal-footer no-bd">
    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
</div>
