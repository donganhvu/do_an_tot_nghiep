@foreach ($data as $value)
<tr>
    <td>{{ $value['id'] }}</td>
    <td>{{ $value['name'] }}</td>
    <td><img src="{{ asset('images/product/' . $value['main_image']) }}"
            style="width:100px; height: 100px;" alt=""></td>
    <td>{{ number_format($value['price'], 0, '', '.') }} VND</td>
    <td>{{ $value['sale'] }}</td>
    <td>{{ $value['views'] }}</td>
    <td>{{ $value['status'] }}</td>
    <td>
        <div class="d-flex">
            <button class="btn show-product" type="button"
                data-url="{{ route('product.show', $value['id']) }}"
                data-toggle="modal" data-target="#show-product"><i
                    class="fas fa-eye text-info font-20"></i></button>
            <button class="btn ml-2 mr-2" type="button"><i
                    class="fas fa-edit text-warning"></i></button>
            <button class="btn delete-product" type="button"
                data-url="{{ route('product.destroy', $value['id']) }}"><i
                    class="fas fa-trash-alt text-danger"></i></button>
        </div>
    </td>
</tr>
@endforeach