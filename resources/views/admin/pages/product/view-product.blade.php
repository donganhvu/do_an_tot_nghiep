@extends('admin-index')
@section('content')
    <div class="container-fluid mt-3">
        <div class="row">
            <div class="col-md-12">
                <!-- Button -->
                <div class="d-flex">
                    <button class="btn btn-primary btn-round mb-3" data-toggle="modal" data-target="#addRowModal">
                        <i class="fa fa-plus"></i> Thêm mới sản phẩm
                    </button>
                </div>

                <!-- Modal -->
                <div class="modal fade" id="addRowModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                    aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 1200px;">
                        <div class="modal-content">
                            <div class="modal-header no-bd">
                                <h5 class="modal-title">
                                    <span class="fw-mediumbold">
                                        Thêm mới</span>
                                    <span class="text-uppercase font-weight-bold text-info">
                                        Sản phẩm
                                    </span>
                                </h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form role="form" id="insert-product" action="" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="category">Danh mục</label>
                                                <select name="category" id="category" class="form-control">
                                                    @foreach ($category as $value)
                                                        <option value="{{ $value['id'] }}">{{ $value['name_cate'] }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="brand">Thương hiệu</label>
                                                <select name="brand" id="brand" class="form-control">
                                                    @foreach ($brand as $value)
                                                        <option value="{{ $value['id'] }}">{{ $value['name_brand'] }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="product-name">Tên sản phẩm</label>
                                                <input type="text" class="form-control" id="product-name"
                                                    name="product-name">
                                            </div>
                                            <div class="form-group">
                                                <label for="image">Ảnh sản phẩm</label>
                                                <input type="file" class="form-control" id="image" name="image"
                                                    accept="image/*">
                                            </div>
                                            <div class="form-group">
                                                <label for="price">Giá bán</label>
                                                <input type="text" class="form-control" id="price" name="price">
                                            </div>
                                            <div class="form-group">
                                                <label for="ckeditor">Mô tả</label>
                                                <textarea name="description" class="form-control ckeditor"
                                                    id="ckeditor"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer no-bd">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                    <button type="submit" id="insert" class="btn btn-primary">Add</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <!-- Table -->
                <div id="load-data">
                    <div class="table-responsive" id="table-data">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Tên sản phẩm</th>
                                    <th>Ảnh sản phẩm</th>
                                    <th>Giá bán</th>
                                    <th>Giảm giá</th>
                                    <th>Lượt xem</th>
                                    <th>Trạng thái</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody id="view-data">
                                @foreach ($data as $value)
                                    <tr>
                                        <td>{{ $value['id'] }}</td>
                                        <td>{{ $value['name'] }}</td>
                                        <td><img src="{{ asset('images/product/' . $value['main_image']) }}"
                                                style="width:100px; height: 100px;" alt=""></td>
                                        <td>{{ number_format($value['price'], 0, '', '.') }} VND</td>
                                        <td>{{ $value['sale'] }}</td>
                                        <td>{{ $value['views'] }}</td>
                                        <td>{{ $value['status'] }}</td>
                                        <td>
                                            <div class="d-flex">
                                                <button class="btn show-product" type="button"
                                                    data-url="{{ route('product.show', $value['id']) }}"
                                                    data-toggle="modal" data-target="#show-product"><i
                                                        class="fas fa-eye text-info font-20"></i></button>
                                                <button class="btn ml-2 mr-2" type="button"><i
                                                        class="fas fa-edit text-warning"></i></button>
                                                <button class="btn delete-product" type="button"
                                                    data-url="{{ route('product.destroy', $value['id']) }}"><i
                                                        class="fas fa-trash-alt text-danger"></i></button>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <nav aria-label="Page navigation example">
                            <ul class="pagination">
                                <li class="page-item">
                                    {{ $data->links() }}
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="show-product" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 1200px;">
            <div class="modal-content" id="load-detail">
               
            </div>
        </div>
    </div>

@endsection
@section('script-ajax')
    <script>
        $(document).ready(function() {
            function load_data() {
                $.ajax({
                    method: "GET",
                    url: "{{ route('product.index') }}",
                    success: function(data) {
                        // $("#load-data").load(" #table-data");
                        $('#view-data').html(data);
                    }
                });
            }

            //* insert product
            $('#insert-product').on('submit', function(e) {
                e.preventDefault();
                var category = $('select[name=category] option').filter(':selected').val(),
                    brand = $('select[name=brand] option').filter(':selected').val(),
                    name = $('#product-name').val(),
                    main_image = $('#image').val(),
                    price = $('#price').val(),
                    description = CKEDITOR.instances["ckeditor"].getData();

                var form_data = new FormData(this);
                form_data.append("category_id", category);
                form_data.append("brand_id", brand);
                form_data.append("name", name);
                form_data.append("main_image", main_image);
                form_data.append("price", price);
                form_data.append("description", description);

                $.ajax({
                    type: "POST",
                    url: "{{ route('product.store') }}",
                    data: form_data,
                    dataType: 'JSON',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(response) {
                        $('#insert').modal('hide');
                        load_data();
                    }
                });
            });

            //* show product
            $('.show-product').click(function() {
                var url = $(this).attr('data-url');
                var _this = $(this);
                $.ajax({
                    type: "GET",
                    url: url,
                    dataType: "html",
                    success: function (data) {
                        $('#load-detail').html(data);
                    }
                });
            });

            //* delete product
            $('.delete-product').click(function() {
                var url = $(this).attr('data-url');
                var _this = $(this);
                $.ajax({
                    type: "DELETE",
                    url: url,
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function() {
                        // load_data();
                        $("#load-data").load(" #table-data");
                        swal({
                            title: "Xóa sản phẩm thành công!",
                            icon: "success",
                            buttons: {
                                confirm: {
                                    text: "Close",
                                    value: true,
                                    visible: true,
                                    className: "btn btn-success",
                                    closeModal: true
                                }
                            }
                        });
                    }
                });
            });
        });

    </script>
@endsection
