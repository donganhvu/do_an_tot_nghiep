<!DOCTYPE html>
<html lang="en">

<head>
    @include('User/includes.head')
</head>

<body>
    <!--================ Start Header Menu Area =================-->
    <header class="header_area">
        @include('User/includes.header')
    </header>
    <!--================ End Header Menu Area =================-->

    <main class="site-main">
      @yield('content')
    </main>


    <!--================ Start footer Area  =================-->
    <footer class="footer">
        @include('User/includes.footer')
    </footer>
    <!--================ End footer Area  =================-->

    @include('User/includes.script')
    @yield('script')
</body>

</html>
