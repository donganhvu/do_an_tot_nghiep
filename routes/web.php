<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//* Route - Admin
Route::group(['prefix' => 'admin'], function(){
    Route::resource('/', 'Admin\Home\HomeController');
    Route::resource('brand', 'Admin\Brand\BrandController');
    Route::resource('category', 'Admin\Product\CategoryController');
    Route::resource('product', 'Admin\Product\ProductController');
});


//* Route - User
Route::group(['prefix' => ''], function(){
    Route::resource('/', 'User\Home\HomeController');
});